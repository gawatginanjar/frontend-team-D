import Vue from 'vue'
import Router from 'vue-router'

import auth from '../services/auth.js'

import LoginComponent from '@/views/Login.vue'
// import SecureComponent from '@/views/secure.vue'

import cmsHome from '@/components/cmsHome'
// import cmsLogin from '@/components/cmsLogin'
import cmsCLientList from '@/components/cmsClientList'
import cmsNewCLient from '@/components/cmsNewClient'
import cmsAgentList from '@/components/cmsAgentList'
import cmsNewAgent from '@/components/cmsNewAgent'
import { Layout } from 'bootstrap-vue/es/components'

Vue.use(Layout)

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: LoginComponent
    },
    {
      path: "/login",
      name: "login",
      component: LoginComponent
    },
    // {
    //   path: "/secure",
    //   name: "secure",
    //   component: SecureComponent
    // },
    {
      name: 'cmsCLientList',
      path: '/client',
      component: cmsCLientList
    },
    {
      name: 'cmsNewCLient',
      path: '/newclient',
      component: cmsNewCLient
    },
    {
      name: 'cmsNewAgent',
      path: '/newagent',
      component: cmsNewAgent
    },
    {
      name: 'cmsAgentList',
      path: '/agent',
      component: cmsAgentList
    },
    {
      path: '/adminhome',
      component: cmsHome,
      children: [

      ]
    },
  ]
})
