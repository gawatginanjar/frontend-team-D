import axios from 'axios';

const api = axios.create({
    // baseURL: 'https://akkruproject.herokuapp.com',
    timeout: 10000,
    params: {} // do not remove this, its added to add params later in the config
});

api.interceptors.request.use(function (config) {
  api.defaults.headers.common['Content-Type'] = 'application/json';
  const token = localStorage.getItem('token');
  if(token) {
    config.headers.Authorization = token;
  }
  return config;
}, function (error) {
    return Promise.reject(error)
})
export default api