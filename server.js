const express = require('express');
const path = require('path');
const serveStatic = require('serve-static');

let app = express();
app.use(serveStatic(__dirname + "/dist"));

app.use('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/dist/index.html'));
});

const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log('Listening on port ' + port)
});


// var express = require('express');
// var path = require('path');
// var serverStatic = require('serve-static');
// app = express();
// app.use(serverStatic(__dirname));
// var port = process.env.PORT || 5000;
// app.listen(port);
// console.log('server started '+ port);